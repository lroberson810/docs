## How to fix a broken stable branch on the GitLab project?

GitLab releases depend on stable branches, as such, it's important to ensure these branches have green pipelines. If a failure in a stable branch is found:

1. Search for an existent issue on the [release tracker]. [GitLab tooling] was built to automatically
   create incident issues when a stable branch fails so chances are one issue is already created.
1. If no issue is found, create a new incident using the [Broken stable branch] template.
1. Follow the steps from the incident issue.
   1. To fix the failure, it is likely a merge request targeting the stable branch will be required on the canonical repository.

Stable branches are protected branches and therefore changes merged into the canonical repository will be automatically propagated to the security and dev repositories.

[release tracker]: https://gitlab.com/gitlab-org/release/tasks/-/issues/?label_name%5B%5D=release-blocker
[GitLab tooling]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/scripts/create-pipeline-failure-incident.rb
[Broken stable branch]: https://gitlab.com/gitlab-org/release/tasks/-/blob/master/.gitlab/issue_templates/Broken-stable-branch.md
