# Overview of the auto-deploy pipelines

The auto-deploy process relies on four [pipelines schedules]:

1. `auto-deploy:prepare`
2. `auto-deploy:tag`
3. `auto-deploy:pick`
4. `auto-deploy:cleanup`

The code definition of these rake tasks can be found on release-tools, particularly
on the [`auto-deploy.rake`] file.

## [`auto-deploy:prepare`]

Creates auto-deploy branches on [GitLab], [Omnibus], [CNG] and [GitLab Chart]
security repositories. The branches are created on security repositories to account
for security releases.

To ensure the commits don't overlap, the branches use the previous auto-deploy tag as
the minimum commit. Several auto-deploy branches are created throughout a day, the
schedule is set at the discretion of release managers at the beginning of their shift.
The current schedule can be seen on the [releases documentation].

As an aside, this task also creates the monthly release issue if one can't be found.

## [`auto-deploy:tag`]

Executed every 15 minutes, this task uses the latest green commit from the current
auto-deploy branches to build an auto-deploy package. Once a package is tagged, a
coordinated pipeline is created and the deployment to our different environment
starts.

When enabled, the [`auto_deploy_tag_latest`] feature flag modifies this task behavior
by forcing the auto-deploy package to be built using the latest commit, regardless of
their status, this behavior is useful for [speedy deployments]. This feature flag is
disabled by default.

## [`auto-deploy:pick`]

Executed every 30 minutes, this task searches for GitLab, Omnibus, CNG, and GitLab
Chart merge requests labeled with `Pick into auto-deploy`, if found, it'll validate
they have a severity label added and then it'll pick them into the respective auto-deploy
branch. For more details, [see our Releases handbook page].

## [`auto-deploy:cleanup`]

Auto-deploy branches are kept for seven days, branches older than that are deleted
by this task that its executed every day at 12UTC.

[`auto-deploy.rake`]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/tasks/auto_deploy.rake
[pipelines schedules]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[releases documentation]: https://about.gitlab.com/handbook/engineering/deployments-and-releases/deployments/#gitlabcom-deployments-process
[GitLab]: https://gitlab.com/gitlab-org/security/gitlab
[Omnibus]: https://gitlab.com/gitlab-org/security/omnibus-gitlab
[CNG]: https://gitlab.com/gitlab-org/security/charts/components/images
[GitLab Chart]: https://gitlab.com/gitlab-org/security/charts
[`auto-deploy:prepare`]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/73/edit
[`auto-deploy:tag`]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/74/edit
[`auto-deploy:pick`]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/72/edit
[`auto-deploy:cleanup`]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/118/edit
[`auto_deploy_tag_latest`]: https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags/191/edit
[speedy deployments]: ../runbooks/how_to_speed_auto_deploy_process_for_urgent_merge_requests.md
[see our Releases handbook page]: https://about.gitlab.com/handbook/engineering/deployments-and-releases/deployments/#gitlabcom-pick-label
[`auto-deploy.rake`]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/tasks/auto_deploy.rake
